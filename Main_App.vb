﻿Imports MySql.Data.MySqlClient
Imports System.IO
Imports System.IO.MemoryStream
Public Class Main_App
    Dim cn As New MySqlConnection
    Dim ds As New DataSet
    Dim da As New MySqlDataAdapter
    Dim cmd As New MySqlCommand
    Dim ms As New MemoryStream
    Dim tabel As New DataTable
    Private Sub koneksinya()
        cn.ConnectionString = "server=175.106.9.60;port=9096;user=unsia_user;password=Unsia_pw;database=unsia_db;allow user variables=true"
    End Sub

    Private Sub DesainDGV()
        Dim SQL As String
        SQL = "SELECT * FROM tbl_mahasiswa ORDER BY nim ASC;"
        cn.Open()
        cmd.Connection = cn
        cmd.CommandText = SQL
        da.SelectCommand = cmd
        tabel.Clear()
        da.Fill(tabel)

        With DGV
            .DataSource = tabel
            .ReadOnly = True
            .AllowUserToAddRows = False
            .AllowUserToDeleteRows = False

            .Columns(0).Width = 130
            .Columns(1).Width = 190
            .Columns(2).Width = 80
            .Columns(3).Width = 100
            .Columns(4).Width = 300

            .Columns(0).HeaderText = "NIM"
            .Columns(1).HeaderText = "Nama"
            .Columns(2).HeaderText = "Jenis Kelamin"
            .Columns(3).HeaderText = "Tanggal Lahir"
            .Columns(4).HeaderText = "Alamat"

            .Columns(0).HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(1).HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(2).HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(3).HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(4).HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter

            .Columns(0).SortMode = DataGridViewColumnSortMode.NotSortable
            .Columns(1).SortMode = DataGridViewColumnSortMode.NotSortable
            .Columns(2).SortMode = DataGridViewColumnSortMode.NotSortable
            .Columns(3).SortMode = DataGridViewColumnSortMode.NotSortable
            .Columns(4).SortMode = DataGridViewColumnSortMode.NotSortable

            .AlternatingRowsDefaultCellStyle.BackColor = Color.Silver
        End With
        cn.Close()

        txt_kode.Clear()
        txt_nama.Clear()
        txt_alamat.Clear()
        cmb_jk.Text = ""
        dtp_tgl_lahir.Value = Now
        txt_kode.Focus()
    End Sub

    Private Sub Form1_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        koneksinya()
        DesainDGV()
        cmb_jk.Text = "- Pilih Salah Satu -"
        TanggalWaktu.Text = "Tanggal " & Date.Today & " Jam " & TimeOfDay

    End Sub

    Private Sub DGV_CellMouseClick(ByVal sender As Object, ByVal e As DataGridViewCellMouseEventArgs) Handles DGV.CellMouseClick
        txt_kode.Text = DGV.CurrentRow.Cells(0).Value
        txt_nama.Text = DGV.CurrentRow.Cells(1).Value
        cmb_jk.Text = DGV.CurrentRow.Cells(2).Value
        dtp_tgl_lahir.Text = DGV.CurrentRow.Cells(3).Value
        txt_alamat.Text = DGV.CurrentRow.Cells(4).Value

        ' Connect to the database
        Dim connectionString As String = "server=175.106.9.60;port=9096;user=unsia_user;password=Unsia_pw;database=unsia_db;allow user variables=true"
        Using connection As New MySqlConnection(connectionString)
            connection.Open()

            ' Retrieve the image data from the database
            Dim SQL As String = "SELECT pas_foto FROM tbl_mahasiswa WHERE nim = @nim"
            Using command As New MySqlCommand(SQL, connection)
                command.Parameters.AddWithValue("@nim", txt_kode.Text)
                Dim imageBytes As Byte() = DirectCast(command.ExecuteScalar(), Byte())

                ' Load the image into the PictureBox3 control
                If imageBytes IsNot Nothing AndAlso imageBytes.Length > 0 Then
                    Using stream As New MemoryStream(imageBytes)
                        PictureBox3.Image = Image.FromStream(stream)
                    End Using
                Else
                    PictureBox3.Image = Nothing
                End If
            End Using
        End Using
    End Sub




    Private Sub SaveButton(ByVal sender As Object, ByVal e As EventArgs) Handles Button1.Click
        Dim SQL As String
        Dim ms As New MemoryStream
        PictureBox3.Image.Save(ms, PictureBox3.Image.RawFormat)

        Try
            If Len(txt_kode.Text) = 0 Then
                MsgBox("Mohon Cek, Form NIM Kosong!")
                txt_kode.Focus()
            ElseIf Len(cmb_jk.Text) = 0 Then
                MsgBox("Mohon Cek, Form Jenis Kelamin Kosong!")
                cmb_jk.Focus()
            Else
                SQL = "INSERT INTO tbl_mahasiswa(nim,nama,jenis_kelamin,tgl_lahir,alamat,pas_foto)"
                SQL += "VALUES('" & Trim(txt_kode.Text) & "','" & Trim(txt_nama.Text) & "',"
                SQL += "'" & Trim(cmb_jk.Text) & "','" & dtp_tgl_lahir.Text & "','" & Trim(txt_alamat.Text) & "','" & Trim(PictureBox3.ToString) & "');"

                cn.Open()
                cmd.Connection = cn
                cmd.CommandText = SQL
                cmd.ExecuteNonQuery()
                cn.Close()
                DesainDGV()
                MsgBox("Berhasil Memasukkan Data")

            End If
        Catch ex As Exception
            MsgBox("Error : " & ex.Message)
            cn.Close()
        End Try
    End Sub

    Private Sub UpdateButton(ByVal sender As Object, ByVal e As EventArgs) Handles Button2.Click
        Dim SQL As String
        Try
            If Len(txt_kode.Text) = 0 Then
                MsgBox("Mohon Cek, Form NIM Kosong!")
                txt_kode.Focus()
            ElseIf Len(cmb_jk.Text) = 0 Then
                MsgBox("Mohon Cek, Form Jenis Kelamin Kosong!")
                cmb_jk.Focus()
            Else
                SQL = "UPDATE tbl_mahasiswa SET "
                SQL += "nama='" & Trim(txt_nama.Text) & "',"
                SQL += "jenis_kelamin='" & Trim(cmb_jk.Text) & "',"
                SQL += "tgl_lahir='" & dtp_tgl_lahir.Text & "',"
                SQL += "alamat='" & Trim(txt_alamat.Text) & "' where nim = '" & txt_kode.Text & "';"


                'MsgBox(SQL)
                cn.Open()
                cmd.Connection = cn
                cmd.CommandText = SQL
                cmd.ExecuteNonQuery()
                cn.Close()
                DesainDGV()
                MsgBox("Berhasil Mengubah Data")
            End If
        Catch ex As Exception
            MsgBox("Error : " & ex.Message)
            cn.Close()
        End Try
    End Sub

    Private Sub DeleteButton(ByVal sender As Object, ByVal e As EventArgs) Handles Button3.Click
        Dim SQL As String
        Try
            If Len(txt_kode.Text) = 0 Then
                MsgBox("Mohon Cek, Form NIM Kosong!")
                txt_kode.Focus()
            ElseIf Len(cmb_jk.Text) = 0 Then
                MsgBox("Mohon Cek, Form Jenis Kelamin Kosong!")
                cmb_jk.Focus()
            Else
                If MsgBox("Kamu yakin ingin menghapus Data ini ?", MsgBoxStyle.YesNo, "Konfirmasi") _
                    = MsgBoxResult.Yes Then
                    SQL = "DELETE FROM tbl_mahasiswa WHERE nim =  '" & Trim(txt_kode.Text) & "'"
                    cn.Open()
                    cmd.Connection = cn
                    cmd.CommandText = SQL
                    cmd.ExecuteNonQuery()
                    cn.Close()
                End If
                DesainDGV()
                MsgBox("Berhasil Menghapus Data")
            End If
        Catch ex As Exception
            MsgBox("Error : " & ex.Message)
            cn.Close()
        End Try
    End Sub

    Private Sub ResetButton(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        txt_nama.Text = ""
        txt_alamat.Text = ""
        txt_kode.Text = ""
        cmb_jk.Text = "- Pilih Salah Satu -"
        dtp_tgl_lahir.Value = Date.Today


        'Application.Exit()
    End Sub

    Private Sub LogoR(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox1.Click
        Process.Start("https://jani.my.id")
    End Sub

    Private Sub LogoUnsia(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox2.Click
        Process.Start("https://unsia.ac.id")
    End Sub

    Private Sub Nama(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label6.Click
        Process.Start("https://jani.my.id")
    End Sub

    Private Sub Unsia(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label8.Click
        Process.Start("https://unsia.ac.id")
    End Sub

    Private Sub WebURL(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label10.Click
        Process.Start("https://jani.my.id")
    End Sub

    Private Sub TanggalJamRealtime(ByVal sender As Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        TanggalWaktu.Text = "Tanggal " & Date.Today & "  - " & " Jam " & TimeOfDay
    End Sub

    Private Sub Label12_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label12.Click

    End Sub

    Private Sub PictureBox3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox3.Click

    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click

        Dim opf As New OpenFileDialog

        opf.Filter = "Choose Image(*.JPG;*.PNG;*.GIF)|*.jpg;*.png;*.gif"

        If opf.ShowDialog = Windows.Forms.DialogResult.OK Then

            PictureBox3.Image = Image.FromFile(opf.FileName)

        End If

    End Sub
End Class